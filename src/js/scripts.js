window.addEventListener('scroll', function() {
   let totalHeight = document.body.scrollHeight - window.innerHeight;
   let windowOffsetHeight = window.pageYOffset;
   let progress = document.querySelector('.progress');
   let percentage = (windowOffsetHeight * 85 / totalHeight)
progress.style.top = percentage + "%";
});


document
.querySelectorAll('.cv__nav a[href^="#"]')
.forEach(trigger => {

    trigger.onclick = function(e) {
        e.preventDefault();
        let hash = this.getAttribute('href');
        let target = document.querySelector(hash);
        let headerOffset = 0
        if (hash == '#contact') {
           headerOffset = -600
        }
        let elementPosition = target.offsetTop;
        let offsetPosition = elementPosition - headerOffset;

        window.scrollTo({
            top: offsetPosition,
            behavior: "smooth"
        });
    };
});
